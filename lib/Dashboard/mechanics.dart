import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:handyman/Dashboard/chatroom.dart';

class MechanicsListPage extends StatefulWidget {
  const MechanicsListPage({Key? key}) : super(key: key);

  @override
  _MechanicsListPageState createState() => _MechanicsListPageState();
}

class _MechanicsListPageState extends State<MechanicsListPage> {
  String name = "";
  String image = "";
  String location = "";
  String profession = "Mechanics";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Center(
          child: Text(
            "Service Providers List",
            style: GoogleFonts.dosis(
              textStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
        ),
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.cyan,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, size: 20, color: Colors.black),
        ),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('providers')
            .where('profession_category', isEqualTo: profession)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              DocumentSnapshot providersData = snapshot.data!.docs[index];
              return Card(
                child: ListTile(
                  title: Text(
                    providersData['full_names'],
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.work_rounded,
                            color: Colors.cyan,
                            size: 20,
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Text(
                            providersData['profession_category'],
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Icon(
                            Icons.location_on,
                            color: Colors.cyan,
                            size: 20,
                          ),
                          SizedBox(
                            width: 2.5,
                          ),
                          Text(
                            providersData['location'],
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                  leading: SizedBox(
                    width: 55,
                    height: 55,
                    child: CircleAvatar(
                      radius: 35,
                      backgroundImage: NetworkImage(
                        providersData['profileImage'],
                      ),
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                  ),
                  onTap: () {
                    Fluttertoast.showToast(
                        msg: "Clicked " +
                            providersData['full_names'] +
                            " " +
                            providersData['profession_category']);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ChatRoom()));
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
