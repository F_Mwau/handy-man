import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:handyman/Dashboard/conversation.dart';
import 'package:handyman/Dashboard/providersConversation.dart';
import 'package:handyman/Dashboard/search.dart';
import 'package:handyman/services/database.dart';

class ProvidersChatRoom extends StatefulWidget {
  const ProvidersChatRoom({Key? key}) : super(key: key);

  @override
  _ProvidersChatRoomState createState() => _ProvidersChatRoomState();
}

class _ProvidersChatRoomState extends State<ProvidersChatRoom> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  final FirebaseAuth auth = FirebaseAuth.instance;
  String myUserName = '';
  Stream? chatRoomsStreams;

  Widget chatRoomList() {
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('ChatRoom')
            .where('users', arrayContains: myUserName)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
              itemCount: snapshot.data!.docs.length,
              itemBuilder: (context, index) {
                return ChatRoomsTile(
                  userName: snapshot.data!.docs[index]
                      .get('chatRoomId')
                      .toString()
                      .replaceAll("_", "")
                      .replaceAll(myUserName, ""),
                  chatRoomId: snapshot.data!.docs[index].get('chatRoomId'),
                );
              });
        });
  }

  getMyUserName() async {
    final User? user = auth.currentUser;
    final uid = user!.uid;
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('providers').doc(uid).get();
    setState(() {
      myUserName = snapshot['full_names'];
      databaseMethods.getChatRooms(myUserName).then((value) {
        setState(() {
          chatRoomsStreams = value;
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getMyUserName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "ChatRoom ",
            style: GoogleFonts.dosis(
              textStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
        ),
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.cyan,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, size: 20, color: Colors.black),
        ),
      ),
      body: chatRoomList(),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.search),
      //   onPressed: () {
      //     Navigator.push(
      //         context, MaterialPageRoute(builder: (context) => SearchPage()));
      //   },
      // ),
    );
  }
}

class ChatRoomsTile extends StatelessWidget {
  const ChatRoomsTile(
      {Key? key, required this.userName, required this.chatRoomId})
      : super(key: key);
  final String userName;
  final String chatRoomId;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ProvidersConversationPage(chatRoomId: chatRoomId)));
      },
      child: Container(
        color: Colors.grey[200],
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(
          children: <Widget>[
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.cyan.shade400,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Text(
                "${userName.substring(0, 1).toUpperCase()}",
                style: GoogleFonts.dosis(
                  textStyle: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),
            ),
            SizedBox(width: 10),
            Text(
              userName,
              style: GoogleFonts.dosis(
                textStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
