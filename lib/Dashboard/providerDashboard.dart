import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:handyman/Dashboard/providersChatRoom.dart';

class ProvidersDashboardPage extends StatefulWidget {
  const ProvidersDashboardPage({Key? key}) : super(key: key);

  @override
  _ProvidersDashboardPageState createState() => _ProvidersDashboardPageState();
}

class _ProvidersDashboardPageState extends State<ProvidersDashboardPage> {
  String myAddress = "";
  String userName = "";
  String userImage = "";
  String workCategory = "";
  String workArea = "";
  String selfDescription = "";
  late double latitude;
  late double longitude;
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    getProviderLocationCoordinates();
    providersData();
  }

  getProviderLocationCoordinates() async {
    final geoLocator = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      latitude = geoLocator.latitude;
      longitude = geoLocator.longitude;
      getAddressBasedOnCoordinates(latitude, longitude);
    });
  }

  getAddressBasedOnCoordinates(double latitude, double longitude) async {
    final coordinates = new Coordinates(latitude, longitude);
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      myAddress = address.first.subLocality;
    });
  }

  providersData() async {
    final User? user = _firebaseAuth.currentUser;
    final uid = user!.uid;
    DocumentSnapshot documentSnapshot =
        await FirebaseFirestore.instance.collection('providers').doc(uid).get();
    print("the providers uid =========>" + uid);
    print("the providers selfDescription =========>" + selfDescription);
    print("the providers workCategory =========>" + workCategory);
    setState(() {
      userName = documentSnapshot['full_names'];
      userImage = documentSnapshot['profileImage'];
      selfDescription = documentSnapshot['bioDescription'];
      workCategory = documentSnapshot['profession_category'];
      workArea = documentSnapshot['location'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.lightBlueAccent, Colors.cyanAccent],
              ),
            ),
            child: Container(
              width: double.infinity,
              height: 200.0,
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      radius: 65.0,
                      backgroundImage: NetworkImage(userImage),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      userName,
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 27,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 16),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: Colors.cyan,
                        size: 25,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        workArea,
                        style: GoogleFonts.dosis(
                          textStyle: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      Icon(
                        Icons.work_rounded,
                        color: Colors.cyan,
                        size: 25,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        workCategory,
                        style: GoogleFonts.dosis(
                          textStyle: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Text(
                    "0712345678",
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      "Self Description",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    selfDescription,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 45,
          ),
          // Button design
          Container(
            padding: EdgeInsets.only(top: 3, left: 3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border(
                bottom: BorderSide(color: Colors.black),
                top: BorderSide(color: Colors.black),
                left: BorderSide(color: Colors.black),
                right: BorderSide(color: Colors.black),
              ),
            ),
            child: MaterialButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProvidersChatRoom()));
                // Fluttertoast.showToast(
                //     msg: "Coming soon ....Chat page under development");
              },
              minWidth: 250,
              height: 50,
              color: Color(0xFF26C6DA),
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
              child: Text(
                "Chat Room",
                style: GoogleFonts.dosis(
                  textStyle: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
