class UserModel {
  String? uid;
  String? fullNames;
  String? email;
  String? imagePath;
  int? phoneNumber;

  UserModel({
    this.uid,
    this.email,
    this.fullNames,
    this.imagePath,
    this.phoneNumber,
  });

  // receiving data from the server
  factory UserModel.fromMap(map) {
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      fullNames: map['full_names'],
      imagePath: map['profileImage'],
      phoneNumber: map['phone_number'],
    );
  }

  // sending data to the server as a Json
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'full_names': fullNames,
      'profileImage': imagePath,
      'phone_number': phoneNumber
    };
  }

  // convert the user object to Json
  Map<String, dynamic> toJson() => {
        'fullNames': fullNames,
        'imagePath': imagePath,
      };

  UserModel copy({
    String? fullNames,
    String? imagePath,
  }) =>
      UserModel(
          imagePath: imagePath ?? this.imagePath,
          fullNames: fullNames ?? this.fullNames);

  static UserModel fromJson(Map<String, dynamic> json) => UserModel(
        imagePath: json['imagePath'],
        fullNames: json['fullNames'],
      );
}
