class ProviderModel {
  String? uid;
  String? fullNames;
  String? email;
  int? phoneNumber;
  String? professionCategory;
  String? description;
  String? imagePath;
  String? location;

  ProviderModel(
      {this.uid,
      this.fullNames,
      this.email,
      this.phoneNumber,
      this.professionCategory,
      this.description,
      this.imagePath,
      this.location});

  factory ProviderModel.fromMap(map) {
    return ProviderModel(
      uid: map['uid'],
      email: map['email'],
      fullNames: map['full_names'],
      phoneNumber: map['phone_number'],
      professionCategory: map['profession_category'],
      location: map['location'],
      description: map['bioDescription'],
      imagePath: map['profileImage'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'full_names': fullNames,
      'phone_number': phoneNumber,
      'profession_category': professionCategory,
      'location': location,
      'bioDescription': description,
      'profileImage': imagePath
    };
  }
}
