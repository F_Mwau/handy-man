class Crafts {
  final String title, work, selfDescription, imageUrl, phoneNumber;

  Crafts(
      {required this.title,
      required this.work,
      required this.phoneNumber,
      required this.selfDescription,
      required this.imageUrl});
}
