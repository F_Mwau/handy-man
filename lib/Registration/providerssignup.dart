import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:handyman/Dashboard/providerDashboard.dart';
import 'package:handyman/Registration/providerslogin.dart';
import 'package:handyman/model/provider_model.dart';
import 'package:image_picker/image_picker.dart';

class ProvidersSignUpPage extends StatefulWidget {
  const ProvidersSignUpPage({Key? key}) : super(key: key);

  @override
  _ProvidersSignUpPageState createState() => _ProvidersSignUpPageState();
}

class _ProvidersSignUpPageState extends State<ProvidersSignUpPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var selectedCategory, selectedType;
  String geoCoordinates = "Locations data";
  late double latitude;
  late double longitude;
  String myAddress = "";
  TextEditingController emailEditingController = new TextEditingController();
  TextEditingController fullNameEditingController = new TextEditingController();
  TextEditingController phoneNumberEditingController =
      new TextEditingController();
  TextEditingController passwordEditingController = new TextEditingController();
  TextEditingController confirmPasswordEditingController =
      new TextEditingController();
  TextEditingController bioTextController = new TextEditingController();
  final picker = ImagePicker();
  File? image;
  String profileUrl = "";

  @override
  void initState() {
    super.initState();
    getUserLocationCoordinates();
  }

  getUserLocationCoordinates() async {
    final geoLocator = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      geoCoordinates =
          "latitude : ${geoLocator.latitude} and  longitude : ${geoLocator.longitude}";
      latitude = geoLocator.latitude;
      longitude = geoLocator.longitude;
      getAddressBasedOnLocation(latitude, longitude);
    });
  }

  getAddressBasedOnLocation(double latitude, double longitude) async {
    final coordinates = new Coordinates(latitude, longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      myAddress = addresses.first.subLocality;
    });
  }

  Future getImageFromGallery() async {
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedImage != null) {
        image = File(pickedImage.path);
      } else {
        Fluttertoast.showToast(msg: "No image selected");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios, size: 20, color: Colors.black),
          ),
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                height: MediaQuery.of(context).size.height * 1.25,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Sign Up",
                          style: GoogleFonts.dosis(
                            textStyle: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Create a service provider account, It's free",
                          style: GoogleFonts.dosis(
                            textStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          width: 130,
                          height: 130,
                          decoration: BoxDecoration(
                            border: Border.all(width: 4, color: Colors.white),
                            boxShadow: [
                              BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                              ),
                            ],
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                            child: image == null
                                ? Text("No image selected")
                                : Image.file(image!),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: InkWell(
                            onTap: () {
                              getImageFromGallery();
                            },
                            child: Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  width: 4,
                                  color: Colors.grey,
                                ),
                                color: Colors.cyan,
                              ),
                              child: Icon(
                                Icons.edit,
                                color: Colors.blueGrey.shade700,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //Full names Edit text design
                          Text(
                            "Full Names",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            obscureText: false,
                            controller: fullNameEditingController,
                            keyboardType: TextInputType.text,
                            validator: (namesValue) {
                              RegExp regExp = new RegExp(r'^.{4,}$');
                              if (namesValue!.isEmpty) {
                                return ("Full Names is required for registration");
                              }
                              if (!regExp.hasMatch(namesValue)) {
                                return ("Please enter valid names(Min. 4 Characters");
                              }
                              return null;
                            },
                            textInputAction: TextInputAction.next,
                            onSaved: (namesValue) {
                              fullNameEditingController.text = namesValue!;
                            },
                            decoration: InputDecoration(
                              prefixIcon:
                                  Icon(Icons.person, color: Colors.cyan),
                              hintText: "John Doe",
                              hintStyle: GoogleFonts.dosis(
                                textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.grey[400],
                                ),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          //Email Edit text design
                          Text(
                            "Email Address",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            obscureText: false,
                            controller: emailEditingController,
                            keyboardType: TextInputType.emailAddress,
                            onSaved: (emailValue) {
                              emailEditingController.text = emailValue!;
                            },
                            validator: (emailValue) {
                              if (emailValue!.isEmpty) {
                                return "Please enter your email address";
                              }
                              if (!RegExp(
                                      "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                                  .hasMatch(emailValue)) {
                                return ("Please Enter a valid email");
                              }
                              return null;
                            },
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.email_outlined,
                                color: Colors.cyan,
                              ),
                              hintText: "john@gmail.com",
                              hintStyle: GoogleFonts.dosis(
                                textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.grey[400],
                                ),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          //Email Edit text design
                          Text(
                            "Phone Number",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            obscureText: false,
                            controller: phoneNumberEditingController,
                            keyboardType: TextInputType.phone,
                            textInputAction: TextInputAction.next,
                            validator: (phoneValue) {
                              RegExp regExp = new RegExp(r'^.{13,}$');
                              if (phoneValue!.isEmpty) {
                                return ("Phone nmber is required for registration");
                              }
                              if (!regExp.hasMatch(phoneValue)) {
                                return ("Please enter valid phone number(Min. 13 Numbers)");
                              }
                              return null;
                            },
                            onSaved: (phoneValue) {
                              phoneNumberEditingController.text = phoneValue!;
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.phone_android_outlined,
                                color: Colors.cyan,
                              ),
                              hintText: "+254 712 345 678",
                              hintStyle: GoogleFonts.dosis(
                                textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.grey[400],
                                ),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Proffession Category",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 3, left: 3),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border(
                                bottom: BorderSide(color: Colors.black45),
                                top: BorderSide(color: Colors.black45),
                                left: BorderSide(color: Colors.black45),
                                right: BorderSide(color: Colors.black45),
                              ),
                            ),
                            child: StreamBuilder<QuerySnapshot>(
                              stream: FirebaseFirestore.instance
                                  .collection("categories")
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  final SnackBar snackBar = SnackBar(
                                    backgroundColor: Colors.white,
                                    content: Text(
                                      "Loading",
                                      style: GoogleFonts.dosis(
                                        textStyle: TextStyle(
                                            fontSize: 22,
                                            color: Colors.black87,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  );
                                  Scaffold.of(context).showSnackBar(snackBar);
                                } else {
                                  List<DropdownMenuItem> categoryItems = [];
                                  for (int i = 0;
                                      i < snapshot.data!.docs.length;
                                      i++) {
                                    DocumentSnapshot snap =
                                        snapshot.data!.docs[i];
                                    categoryItems.add(
                                      DropdownMenuItem(
                                        child: Text(
                                          snap.id,
                                          style: GoogleFonts.dosis(
                                            textStyle: TextStyle(
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        value: "${snap.id}",
                                      ),
                                    );
                                  }
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.badge,
                                        color: Colors.cyan,
                                      ),
                                      SizedBox(
                                        width: 12,
                                      ),
                                      DropdownButton<dynamic>(
                                        items: categoryItems,
                                        onChanged: (categoriesValue) {
                                          final SnackBar bar = SnackBar(
                                            backgroundColor: Colors.cyan,
                                            content: Text(
                                              "Selected category is $categoriesValue",
                                              style: GoogleFonts.dosis(
                                                textStyle: TextStyle(
                                                    fontSize: 22,
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          );
                                          Scaffold.of(context)
                                              .showSnackBar(bar);
                                          setState(() {
                                            // selected category is the one saved to the db of service providers
                                            selectedCategory = categoriesValue;
                                          });
                                        },
                                        value: selectedCategory,
                                        isExpanded: false,
                                        hint: new Text(
                                          "Choose a profession category",
                                          style: GoogleFonts.dosis(
                                            textStyle: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.grey[400],
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  );
                                }
                                return selectedCategory;
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "My Location",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border(
                                bottom: BorderSide(color: Colors.black45),
                                top: BorderSide(color: Colors.black45),
                                left: BorderSide(color: Colors.black45),
                                right: BorderSide(color: Colors.black45),
                              ),
                            ),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  color: Colors.cyan,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  myAddress,
                                  style: GoogleFonts.dosis(
                                    textStyle: TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          // Bio input field
                          Text(
                            "Enter some info about your work ethic",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            minLines: 2,
                            maxLines: 6,
                            keyboardType: TextInputType.multiline,
                            controller: bioTextController,
                            validator: (value) {
                              RegExp regExp = new RegExp(r'v.{4,}$');
                              if (value!.isEmpty) {
                                return ("Your bio needs some data to be saved");
                              }
                              if (!regExp.hasMatch(value)) {
                                return ("Please enter a length bio description (Min. 10 characters)");
                              }
                            },
                            onSaved: (value) {
                              bioTextController.text = value!;
                            },
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              hintText: "Enter your bio",
                              hintStyle: GoogleFonts.dosis(
                                textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.grey[400],
                                ),
                              ),
                              prefixIcon: Icon(
                                Icons.article,
                                color: Colors.cyan,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          //Password Edit text design
                          Text(
                            "Password",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            obscureText: true,
                            controller: passwordEditingController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            validator: (passwordValue) {
                              RegExp regExp = new RegExp(r'^.{6,}$');
                              if (passwordValue!.isEmpty) {
                                return ("Password is required for login");
                              }
                              if (!regExp.hasMatch(passwordValue)) {
                                return ("Please enter valid password(Min. 6 Characters)");
                              }
                            },
                            onSaved: (passwordValue) {
                              passwordEditingController.text = passwordValue!;
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                color: Colors.cyan,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Confirm Password",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextFormField(
                            obscureText: true,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                            controller: confirmPasswordEditingController,
                            onSaved: (confirmValue) {
                              confirmPasswordEditingController.text =
                                  confirmValue!;
                            },
                            validator: (confirmValue) {
                              if (confirmPasswordEditingController.text.length >
                                      6 &&
                                  passwordEditingController.text !=
                                      confirmValue) {
                                return ("Passwords do not match.");
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                color: Colors.cyan,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                        ],
                      ),
                    ),
                    // Button design
                    Container(
                      padding: EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        border: Border(
                          bottom: BorderSide(color: Colors.black),
                          top: BorderSide(color: Colors.black),
                          left: BorderSide(color: Colors.black),
                          right: BorderSide(color: Colors.black),
                        ),
                      ),
                      child: MaterialButton(
                        onPressed: () {
                          signUp(emailEditingController.text,
                              passwordEditingController.text);
                        },
                        minWidth: double.infinity,
                        height: 60,
                        color: Color(0xFF26C6DA),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Text(
                          "Sign up",
                          style: GoogleFonts.dosis(
                            textStyle: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Already have an account?",
                          style: GoogleFonts.dosis(
                            textStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ProvidersLogInPage()));
                          },
                          child: Text(
                            "Log In",
                            style: GoogleFonts.dosis(
                              textStyle: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.cyan),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  void signUp(String email, String password) async {
    if (formKey.currentState!.validate()) {
      try {
        await _auth
            .createUserWithEmailAndPassword(email: email, password: password)
            .then((value) => {uploadImageToStorage()});
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString().trim());
      }
    }
  }

  uploadImageToStorage() async {
    User? user = _auth.currentUser;
    var imageFile = FirebaseStorage.instance
        .ref()
        .child("providers")
        .child(user!.uid + ".jpg");
    UploadTask task = imageFile.putFile(image!);
    TaskSnapshot snapshot = await task;
    profileUrl = await snapshot.ref.getDownloadURL();
    print("Image download url ===============>" + profileUrl);
    postDetailsToFireStore(profileUrl, user);
  }

  postDetailsToFireStore(String imageUrl, User? providerUser) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    ProviderModel providerModel = ProviderModel();

    // writes
    providerModel.email = providerUser!.email;
    providerModel.uid = providerUser.uid;
    providerModel.fullNames = fullNameEditingController.text;
    providerModel.location = myAddress;
    providerModel.description = bioTextController.text;
    providerModel.imagePath = imageUrl;
    providerModel.professionCategory = selectedCategory;
    providerModel.phoneNumber = int.parse(phoneNumberEditingController.text);

    await firestore
        .collection("providers")
        .doc(providerUser.uid)
        .set(providerModel.toMap());

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ProvidersDashboardPage()));
    // Navigator.of(context)
    //     .pushReplacement(MaterialPageRoute(builder: (context) => Dashboard()));
    Fluttertoast.showToast(msg: "Account created successfully !!");
  }
}
