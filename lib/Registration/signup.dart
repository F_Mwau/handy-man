import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:handyman/Dashboard/dashboard.dart';
import 'package:handyman/Registration/login.dart';
import 'package:handyman/model/user_model.dart';
import 'package:image_picker/image_picker.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController fullNameEditingController = new TextEditingController();
  TextEditingController emailEditingController = new TextEditingController();
  TextEditingController passwordEditingController = new TextEditingController();
  TextEditingController confirmPasswordEditingController =
      new TextEditingController();
  TextEditingController phoneNumberEditingController =
      new TextEditingController();
  final picker = ImagePicker();
  File? image;
  String profileUrl = "";

  Future getImageFromGallery() async {
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedImage != null) {
        image = File(pickedImage.path);
      } else {
        Fluttertoast.showToast(msg: "No image selected");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, size: 20, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          height: MediaQuery.of(context).size.height - 50,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    "Sign Up",
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Create an account, It's free",
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey[700]),
                    ),
                  ),
                ],
              ),
              Stack(
                children: <Widget>[
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                      border: Border.all(width: 4, color: Colors.white),
                      boxShadow: [
                        BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 10,
                          color: Colors.black.withOpacity(0.1),
                        ),
                      ],
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: image == null
                          ? Text("No image selected")
                          : Image.file(image!),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: InkWell(
                      onTap: () {
                        getImageFromGallery();
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 4, color: Colors.grey),
                          color: Colors.cyan,
                        ),
                        child: Icon(
                          Icons.edit,
                          color: Colors.blueGrey.shade700,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //Full names Edit text design
                    Text(
                      "Full Names",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      obscureText: false,
                      controller: fullNameEditingController,
                      keyboardType: TextInputType.text,
                      validator: (namesValue) {
                        RegExp regExp = new RegExp(r'^.{4,}$');
                        if (namesValue!.isEmpty) {
                          return ("Full Names is required for registration");
                        }
                        if (!regExp.hasMatch(namesValue)) {
                          return ("Please enter valid names(Min. 4 Characters");
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.next,
                      onSaved: (namesValue) {
                        fullNameEditingController.text = namesValue!;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person, color: Colors.cyan),
                        hintText: "John Doe",
                        hintStyle: GoogleFonts.dosis(
                          textStyle: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[400],
                          ),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    //Email Edit text design
                    Text(
                      "Email Address",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      obscureText: false,
                      controller: emailEditingController,
                      keyboardType: TextInputType.emailAddress,
                      onSaved: (emailValue) {
                        emailEditingController.text = emailValue!;
                      },
                      validator: (emailValue) {
                        if (emailValue!.isEmpty) {
                          return "Please enter your email address";
                        }
                        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                            .hasMatch(emailValue)) {
                          return ("Please Enter a valid email");
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.email_outlined,
                          color: Colors.cyan,
                        ),
                        hintText: "john@gmail.com",
                        hintStyle: GoogleFonts.dosis(
                          textStyle: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[400],
                          ),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    //Email Edit text design
                    Text(
                      "Phone Number",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      obscureText: false,
                      controller: phoneNumberEditingController,
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      validator: (phoneValue) {
                        RegExp regExp = new RegExp(r'^.{13,}$');
                        if (phoneValue!.isEmpty) {
                          return ("Phone nmber is required for registration");
                        }
                        if (!regExp.hasMatch(phoneValue)) {
                          return ("Please enter valid phone number(Min. 13 Numbers)");
                        }
                        return null;
                      },
                      onSaved: (phoneValue) {
                        phoneNumberEditingController.text = phoneValue!;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.phone_android_outlined,
                          color: Colors.cyan,
                        ),
                        hintText: "+254 712 345 678",
                        hintStyle: GoogleFonts.dosis(
                          textStyle: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[400],
                          ),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    //Password Edit text design
                    Text(
                      "Password",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      obscureText: true,
                      controller: passwordEditingController,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      validator: (passwordValue) {
                        RegExp regExp = new RegExp(r'^.{6,}$');
                        if (passwordValue!.isEmpty) {
                          return ("Password is required for login");
                        }
                        if (!regExp.hasMatch(passwordValue)) {
                          return ("Please enter valid password(Min. 6 Characters)");
                        }
                      },
                      onSaved: (passwordValue) {
                        passwordEditingController.text = passwordValue!;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.vpn_key,
                          color: Colors.cyan,
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Confirm Password",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      obscureText: true,
                      controller: confirmPasswordEditingController,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      onSaved: (confirmValue) {
                        confirmPasswordEditingController.text = confirmValue!;
                      },
                      validator: (confirmValue) {
                        if (confirmPasswordEditingController.text.length > 6 &&
                            passwordEditingController.text != confirmValue) {
                          return ("Passwords do not match.");
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.vpn_key,
                          color: Colors.cyan,
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade400),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
              // Button design
              Container(
                padding: EdgeInsets.only(top: 3, left: 3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border(
                    bottom: BorderSide(color: Colors.black),
                    top: BorderSide(color: Colors.black),
                    left: BorderSide(color: Colors.black),
                    right: BorderSide(color: Colors.black),
                  ),
                ),
                child: MaterialButton(
                  onPressed: () {
                    signUp(emailEditingController.text,
                        passwordEditingController.text);
                  },
                  minWidth: double.infinity,
                  height: 60,
                  color: Color(0xFF26C6DA),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: Text(
                    "Sign up",
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Already have an account?",
                    style: GoogleFonts.dosis(
                      textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey[700]),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text(
                      "Log In",
                      style: GoogleFonts.dosis(
                        textStyle: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.cyan),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void signUp(String email, String password) async {
    if (formKey.currentState!.validate()) {}
    try {
      await _auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) => {uploadImageToStorage()});
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString().trim());
    }
  }

  uploadImageToStorage() async {
    User? user = _auth.currentUser;
    var imageFile =
        FirebaseStorage.instance.ref().child("users").child(user!.uid + ".jpg");
    UploadTask task = imageFile.putFile(image!);
    TaskSnapshot snapshot = await task;
    profileUrl = await snapshot.ref.getDownloadURL();
    print("USer profile picture Url ==============>" + profileUrl);
    Fluttertoast.showToast(msg: "Image Uploaded");
    postDetailsToFireStore(profileUrl, user);
  }

  postDetailsToFireStore(String imageUrl, User? user) async {
    // calling out firestore and user model then sending the values
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    // User? user = _auth.currentUser;

    UserModel userModel = UserModel();

    // writing on the values
    userModel.email = user!.email;
    userModel.uid = user.uid;
    userModel.fullNames = fullNameEditingController.text;
    userModel.imagePath = imageUrl;
    userModel.phoneNumber = int.parse(phoneNumberEditingController.text);

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(userModel.toMap());
    // Navigator.of(context)
    //     .pushReplacement(MaterialPageRoute(builder: (context) => Dashboard()));
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Dashboard()));
    Fluttertoast.showToast(msg: "Account created successfully !!");
  }
}
